#pragma once

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <stdlib.h>
#include <cstdint>
#include <fstream>
#include <vector>
#include <Psapi.h>
#include <time.h>

#include "inc/natives.h"
#include "inc/enums.h"
#include "inc/keyboard.h"