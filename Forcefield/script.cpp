#include "stdafx.h"
#include "inc/main.h"

#include "script.h"

Ped GetPlayerPed()
{
	if (PLAYER::DOES_MAIN_PLAYER_EXIST())
	{
		return PLAYER::GET_PLAYER_PED(PLAYER::GET_PLAYER_ID());
	}

	return NULL;
}

bool GetPedIsEnemy(Ped playerPed, Ped ped)
{
	Group plrPedGroup = PED::GET_PED_GROUP_INDEX(playerPed);

	if (PED::IS_PED_IN_GROUP(ped, plrPedGroup)) return false;

	Vector3 pedPos = PED::GET_PED_COORDS(ped);

	Ped closestEnemyPed;
	PED::GET_CLOSEST_ENEMY_PED(playerPed, pedPos.x, pedPos.y, pedPos.z, 2.0f, true, true, false, true, false, true, &closestEnemyPed);

	if (closestEnemyPed == NULL || !PED::DOES_PED_EXIST(closestEnemyPed) || ped != closestEnemyPed) return false;

	return true;
}

void main()
{
	bool enabled = GetPrivateProfileInt(L"SETTINGS", L"ENABLED_BY_DEFAULT", 0, L".\\Forcefield.ini") != 0;
	int toggleKey = GetPrivateProfileInt(L"SETTINGS", L"TOGGLE_KEY", 0x4C, L".\\Forcefield.ini"); //default is L
	float radius = (float)GetPrivateProfileInt(L"SETTINGS", L"RADIUS", 15, L".\\Forcefield.ini");
	bool onlyEnemies = GetPrivateProfileInt(L"SETTINGS", L"ONLY_ENEMIES", 1, L".\\Forcefield.ini") != 0;

	int lastGetPeds = 0, lastGetVehs = 0;
	int numPeds = 0, numVehs = 0;
	Ped worldPeds[16];
	Vehicle worldVehs[16];

	while (true)
	{
		if (!HUD::IS_PAUSE_MENU_ACTIVE() && CUTSCENE::GET_CUTSCENE_TIME_MS() == 0)
		{
			if (IsKeyJustUp(toggleKey))
			{
				enabled = !enabled;

				const char *printStr;
				if (enabled) printStr = "Forcefield enabled";
				else printStr = "Forcefield disabled";

				HUD::PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", printStr, 1000, 1);
			}

			if (enabled)
			{
				Ped plrPed = GetPlayerPed();

				if (plrPed != NULL)
				{
					int gameTime = MISC::GET_GAME_TIMER();

					if (gameTime - lastGetPeds >= 1000)
					{
						numPeds = worldGetAllPeds(worldPeds, 16);
						lastGetPeds = gameTime;
					}

					if (gameTime - lastGetVehs >= 1500)
					{
						numVehs = worldGetAllVehicles(worldVehs, 16);
						lastGetVehs = gameTime;
					}

					Vector3 plrPedPos = PED::GET_PED_COORDS(plrPed);

					for (int i = 0; i < numPeds; i++)
					{
						if (worldPeds[i] != NULL && PED::DOES_PED_EXIST(worldPeds[i]) && !PED::IS_PED_DEAD(worldPeds[i]) && worldPeds[i] != plrPed && (!onlyEnemies || GetPedIsEnemy(plrPed, worldPeds[i])))
						{
							Vector3 pedPos = PED::GET_PED_COORDS(worldPeds[i]);

							if (MISC::GET_DISTANCE_BETWEEN_COORDS(pedPos.x, pedPos.y, pedPos.z, plrPedPos.x, plrPedPos.y, plrPedPos.z, true) <= radius)
							{
								Vector3 direction = (pedPos - plrPedPos).normalize() * 150.0f;
								direction.z = 100.0f;

								PED::SET_PED_TO_RAGDOLL(worldPeds[i], 3000, 3000, 0, false, false, true, -1056964608);
								PED::APPLY_FORCE_TO_PED(worldPeds[i], 1, direction.x, direction.y, direction.z, 0.0f, 0.0f, 0.0f, (int)ePedBone::ROOT, false, false, false);
							}
						}

						scriptWait(0);
					}

					for (int i = 0; i < numVehs; i++)
					{
						if (worldVehs[i] != NULL && VEHICLE::DOES_VEHICLE_EXIST(worldVehs[i]) && !VEHICLE::IS_VEHICLE_DEAD(worldVehs[i]))
						{
							Vector3 vehPos = VEHICLE::GET_VEHICLE_COORDS(worldVehs[i]);

							if (MISC::GET_DISTANCE_BETWEEN_COORDS(vehPos.x, vehPos.y, vehPos.z, plrPedPos.x, plrPedPos.y, plrPedPos.z, true) <= radius)
							{
								Vector3 direction = (vehPos - plrPedPos).normalize() * 200.0f;
								direction.z = 250.0f;

								VEHICLE::APPLY_FORCE_TO_VEHICLE(worldVehs[i], 2, direction.x, direction.y, direction.z, direction.x / 3.0f, direction.y / 3.0f, direction.z / 3.0f, 0, false, false, true);
							}
						}

						scriptWait(0);
					}
				}
			}
		}

		scriptWait(0);
	}
}

void ScriptMain()
{
	srand(GetTickCount());
	main();
}
